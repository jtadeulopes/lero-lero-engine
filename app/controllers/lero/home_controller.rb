# encoding: utf-8

require_dependency "lero/application_controller"

module Lero
  class HomeController < ApplicationController
    def index
      txt = []
      txt << "É importante questionar o quanto o início da atividade geral de formação de atitudes nos obriga à análise do fluxo de informações."
      txt << "Por conseguinte, a estrutura atual da organização obstaculiza a apreciação da importância de alternativas às soluções ortodoxas."
      txt << "A nível organizacional, a expansão dos mercados mundiais é uma das consequências do remanejamento dos quadros funcionais."
      txt << "O cuidado em identificar pontos críticos na complexidade dos estudos efetuados talvez venha a ressaltar a relatividade do retorno esperado a longo prazo."
      txt << "A prática cotidiana prova que a hegemonia do ambiente político não pode mais se dissociar dos paradigmas corporativos."
      txt << "Caros amigos, o acompanhamento das preferências de consumo cumpre um papel essencial na formulação das diversas correntes de pensamento."
      txt << "As experiências acumuladas demonstram que a crescente influência da mídia auxilia a preparação e a composição da gestão inovadora da qual fazemos parte."
      txt << "Podemos já vislumbrar o modo pelo qual o desenvolvimento contínuo de distintas formas de atuação cumpre um papel essencial na formulação do investimento em reciclagem técnica."
      render :text => txt.sort_by {rand}.first
    end
  end
end
