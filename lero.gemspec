$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "lero/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "lero"
  s.version     = Lero::VERSION
  s.authors     = ["Jésus Lopes"]
  s.email       = ["jlopes@zigotto.com.br"]
  s.homepage    = "http://www.lerolero.com"
  s.summary     = "Lero engine"
  s.description = "Lero engine"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.6"

  s.add_development_dependency "sqlite3"
end
